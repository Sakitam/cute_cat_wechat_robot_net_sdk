﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using spin100_Keaimao_SDK.Comply;

namespace spin100_Keaimao_SDK
{
    public partial class Main
    {
        public static Student one = new Student();

        [DllExport(ExportName = "LoadingInfo", CallingConvention = CallingConvention.StdCall)]
        public static int LoadingInfo(int session)
        {
            one.name = "Sign";
            one.desc = "Desc";
            one.author = "Author";
            one.version = "1.0.0";
            one.api_version = "4.1";
            one.menu_title = "Config";
            one.cover_base64 = API.Imgbase64;
            one.developer_key = "818198829";
            var jsonData = JsonConvert.SerializeObject(one);
            API.Auth_code = API.Initialize(session, jsonData);
            return API.Auth_code;
        }

        [DllExport(ExportName = "EventInit", CallingConvention = CallingConvention.StdCall)]
        public static int EventInit()
        {
            SaveConfig();
            return 0;
        }

        [DllExport(ExportName = "EventEnable", CallingConvention = CallingConvention.StdCall)]
        public static int EventEnable()
        {
            LoadConfig();
            return 0;
        }

        [DllExport(ExportName = "EventStop", CallingConvention = CallingConvention.StdCall)]
        public static int EventStop()
        {
            SaveConfig();
            return 0;
        }

        [DllExport(ExportName = "EventLogin", CallingConvention = CallingConvention.StdCall)]
        public static int EventLogin(string rob_wxid, string rob_wxname, int type)
        {
            return 0;
        }

        /*
              robot_wxid  机器人帐号ID
              type        消息类型，1文本，3图片，34语音，42名片，43视频，47动态表情，48地理位置，49分享链接，2001红包，2002小程序，2003群邀请
              from_wxid   来源群ID
              from_name   来源群昵称
              final_from_wxid   具体发群消息的成员ID
              final_from_name   具体发群消息的成员昵称
              to_wxid     接收信息的人的ID，（一般是机器人收到消息，所以是机器人的id,如果是机器人主动发消息给别人，那就是别人的id）
              Msg         群消息内容
            */
        [DllExport(ExportName = "EventGroupMsg", CallingConvention = CallingConvention.StdCall)]
        public static int EventGroupMsg(string robot_wxid, int type, string from_wxid, string from_name,
            string final_from_wxid, string final_from_name, string to_wxid, string Msg)
        {
            if (type == 1 && Msg == "checkId")
                API.SendTextMsg(robot_wxid, from_wxid, from_wxid);

            if (type == 1 && Msg.Contains("签到"))
            {
                var str = Msg.Substring(2);

                foreach (var groupSign in Instance.GroupSignsList)
                {
                    if (groupSign.GroupId == from_wxid)
                    {
                        if (groupSign.IsOpen == false)
                        {
                            API.SendTextMsg(robot_wxid, from_wxid, "目前没有到签到时间。");
                        }

                        if (groupSign.IsOpen)
                        {
                            if (groupSign.NameList.Contains(str))
                            {
                                groupSign.CurNum += 1;
                                groupSign.SignedNameList.Add(str);
                                groupSign.NameList.Remove(str);
                                API.SendTextMsg(robot_wxid, from_wxid, str+"，签到成功。");
                                SaveConfig();
                                return 0;
                            }
                           
                            if (groupSign.SignedNameList.Contains(str))
                            {
                                API.SendTextMsg(robot_wxid, from_wxid, str+"，请勿重复签到");
                                SaveConfig();
                                return 0;
                            }

                            API.SendTextMsg(robot_wxid, from_name, "你不在我们的签到名单内噢");
                        }
                    }
                }
            }
            
            SaveConfig();
            return 0;
        }

        /*
            robot_wxid  机器人帐号ID
            type        消息类型，1文本，3图片，34语音，42名片，43视频，47动态表情，48地理位置，49分享链接，2001红包，2002小程序，2003群邀请
            from_wxid  来源用户ID
            from_name  来源用户昵称
            to_wxid     接收信息的人的ID，（一般是机器人收到消息，所以是机器人的id,如果是机器人主动发消息给别人，那就是别人的id）
            Msg         群消息内容
            */
        [DllExport(ExportName = "EventFriendMsg", CallingConvention = CallingConvention.StdCall)]
        public static int EventFriendMsg(string robot_wxid, int type, string from_wxid, string from_name,
            string to_wxid, string Msg)
        {
            if (type == 1 && Msg == "checkId") API.SendTextMsg(robot_wxid, from_wxid, from_wxid);

            if (type == 1 && Msg == "getAdmin")
            {
                SetAdmin(from_wxid);
                API.SendTextMsg(robot_wxid,from_wxid,"管理员id:"+from_wxid);
            }

            if (type == 1 && Msg == "getSuperAdmin") SetSuperAdmin(from_wxid);

            // 创建签到:wxid,人数
            if (type == 1 && Msg.Contains("创建签到:") && IsAdmin(from_wxid))
            {
                var str = Msg.Substring(5);
                var items = str.Split(',');
                var groupSign = new GroupSign
                {
                    GroupId = items[0],
                    AdminId = from_wxid,
                    TotleNum = items[1],
                    CurNum = 0.ToString(),
                    IsOpen = false,
                    NameList = new List<string>(),
                    SignedNameList = new List<string>()
                };
                Instance.GroupSignsList.Add(groupSign);

                API.SendTextMsg(robot_wxid, from_wxid, "创建成功，总人数" + groupSign.TotleNum + "人，目前签到人数" + groupSign.CurNum +
                                                       "人。群组id" + groupSign.GroupId);
            }

            // 绑定学生:名字1,名字2,名字3
            // 最后不要打句号什么之类的
            if (type == 1 && Msg.Contains("绑定学生:") && IsAdmin(from_wxid))
            {
                var str = Msg.Substring(5);
                var nameLists = str.Split(',');
                foreach (var groupSign in Instance.GroupSignsList)
                {
                    if (groupSign.AdminId == from_wxid)
                    {
                        groupSign.NameList = new List<string>();
                        foreach (var item in nameLists) groupSign.NameList.Add(item);
                    }

                    var names = "";
                    foreach (var item in groupSign.NameList)
                    {
                        names += item+",";
                    }
                    API.SendTextMsg(robot_wxid, from_wxid, "目前学生名单:"+names);
                }
            }

            if (type == 1 && Msg.Contains("开始签到") && IsAdmin(from_wxid))
            {
                foreach (var groupSign in Instance.GroupSignsList)
                {
                    if (groupSign.AdminId == from_wxid)
                    {
                        groupSign.CurNum = 0.ToString();
                        groupSign.SignedNameList = new List<string>();
                        groupSign.IsOpen = true;
                    }
                }

                API.SendTextMsg(robot_wxid, from_wxid, "开始签到！");
            }

            if (type == 1 && Msg.Contains("结束签到") && IsAdmin(from_wxid))
            {
                foreach (var groupSign in Instance.GroupSignsList)
                {
                    if (groupSign.AdminId == from_wxid)
                    {
                        groupSign.IsOpen = false;
                    }
                }

                API.SendTextMsg(robot_wxid, from_wxid, "结束签到！");
            }

            if (type == 1 && Msg.Contains("查看签到") && IsAdmin(from_wxid))
            {
                var str = "未签到学生名单：";
                foreach (var groupSign in Instance.GroupSignsList)
                {
                    foreach (var item in groupSign.NameList)
                    {
                        str = str + item + ",";
                    }

                    str += "已签到"+groupSign.CurNum+"人,未签到"+(Convert.ToInt32(groupSign.TotleNum) -Convert.ToInt32(groupSign.CurNum))+"人。";
                    API.SendTextMsg(robot_wxid, from_wxid, str);
                }
            }

            

            SaveConfig();
            return 0;
        }

        /*
             robot_wxid  机器人帐号ID
             from_wxid  来源用户ID
             from_name   来源用户昵称
             to_wxid
             money  金额
             Msg_json    json格式数据消息，请自行解析
             */
        [DllExport(ExportName = "EventReceivedTransfer", CallingConvention = CallingConvention.StdCall)]
        public static int EventReceivedTransfer(string robot_wxid, string from_wxid, string from_name, string to_wxid,
            string money, string Msg_json)
        {
            if (API.JsonMsg(Msg_json)["is_arrived"].ToString() == "1") //判断是否为即时转账类型，因为有可能是延迟转账类型，1即时转账，0延迟转账
            {
                if (API.JsonMsg(Msg_json)["is_received"].ToString() == "0") //等于0 就是还没收钱
                    API.AcceptTransfer(robot_wxid, from_wxid, Msg_json); //调用收钱API 接收好友转账

                if (API.JsonMsg(Msg_json)["is_received"].ToString() == "1") //收款成功,收钱后第二次事件to_wxid才是原来转账的用户ID
                {
                    //这个时候给你转账的用户ID为=to_wxid
                    //根据ID查用户名，调用方法  API.nickname(robot_wxid, to_wxid)
                }
            }

            return 0;
        }

        /*
             robot_wxid  收钱的人帐号ID
             pay_wxid   来源用户ID
             pay_name  来源用户昵称
             money  金额
             Msg_json   更多数据：json数据格式数据消息，请自行解析
             */
        [DllExport(ExportName = "EventScanCashMoney", CallingConvention = CallingConvention.StdCall)]
        public static int EventScanCashMoney(string robot_wxid, string pay_wxid, string pay_name, string money,
            string Msg_json)
        {
            return 0;
        }

        /*
             robot_wxid  机器人帐号ID
             from_wxid   陌生人用户ID
             from_name   陌生人用户昵称
             to_wxid           忽略
             Msg_json    (详细好友验证信息：1群内添加时候包含群ID，2名片推荐添加时，包含推荐人ID和名称，3w微信号，手机号搜索时候添加)
            */
        [DllExport(ExportName = "EventFriendVerify", CallingConvention = CallingConvention.StdCall)]
        public static int EventFriendVerify(string robot_wxid, string from_wxid, string from_name, string to_wxid,
            string Msg_json)
        {
            API.AgreeFriendVerify(robot_wxid, Msg_json);

            return 0;
        }

        [DllExport(ExportName = "EventGroupMemberAdd", CallingConvention = CallingConvention.StdCall)]
        public static int
            EventGroupMemberAdd(string robot_wxid, string from_wxid, string from_name, string Msg_json)
        {
            return 0;
        }

        /*
           robot_wxid  机器人帐号ID
           from_wxid   来源群ID
           from_name   来源群昵称
           Msg    退出人id|退出人昵称
          */
        [DllExport(ExportName = "EventGroupMemberDecrease", CallingConvention = CallingConvention.StdCall)]
        public static int EventGroupMemberDecrease(string robot_wxid, string from_wxid, string from_name,
            string Msg_json)
        {
            return 0;
        }

        /*
          robot_wxid  机器人帐号ID
         
          Msg_json   {"type","1"} 1 已经不是好友，2已经被对方拉黑
           */
        [DllExport(ExportName = "EventSysMsg", CallingConvention = CallingConvention.StdCall)]
        public static int EventSysMsg(string robot_wxid, int type, string Msg_json)
        {
            return 0;
        }

        [DllExport(ExportName = "Menu", CallingConvention = CallingConvention.StdCall)]
        public static int Menu()
        {
            var form = new spin100();
            var dr = form.ShowDialog();

            return 0;
        }

        /*
         返回 0 继续执行下一个插件的事件
         返回 1 执行完当前插件，不再执行下一个插件了

         消息类型：
         1  文本
         3 图片
         34 语音
         37 好友验证
         42 名片
         43 视频
         47 动画表情
         48 位置
         49 链接
        2000 转账
        2001  红包
        2002 小程序
        2003 群邀请
        2004 文件
        10000 系统消息 
        10002 服务通知 
         */
    }
}