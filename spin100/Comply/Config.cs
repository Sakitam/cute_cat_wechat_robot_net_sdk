﻿//==============================================================
//  Create by Sakitam_Ins at 2020/2/1 16:48:41.
//  Version 1.0
//  Sakitam_Ins [tjwsq-qiqiqi@outlook.com]
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace spin100_Keaimao_SDK.Comply
{
    public static class Config
    {
        public static Fileini ConfigFile = new Fileini();

        public static void InitConfig()
        {
            ConfigFile.WriteValue("GroupList","Group",JsonConvert.SerializeObject(new GroupSign()));
        }
    }
}
