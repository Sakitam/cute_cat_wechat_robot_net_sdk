﻿//==============================================================
//  Create by Sakitam_Ins at 2020/2/1 21:41:59.
//  Version 1.0
//  Sakitam_Ins [tjwsq-qiqiqi@outlook.com]
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Newtonsoft.Json;
using spin100_Keaimao_SDK.Comply;

namespace spin100_Keaimao_SDK
{
    public partial class Main
    {
        public static Fileini Config = new Fileini();
        public static ConfigInstance Instance = new ConfigInstance
        {
            Admin = new List<string>(), 
            SuperAdmin = new List<string>(),
            GroupSignsList = new List<GroupSign>()
        };

        public static void SaveConfig()
        {
            Config.WriteValue("Config", "Instance", JsonConvert.SerializeObject(Instance));
        }

        public static void LoadConfig()
        {
            Instance = JsonConvert.DeserializeObject<ConfigInstance>(Config.ReadValue("Config", "Instance"));
        }

        public static bool IsAdmin(string id)
        {
            return Instance.Admin.Contains(id);
        }

        public static bool IsSuperAdmin(string id)
        {
            return Instance.SuperAdmin.Contains(id);
        }

        public static void SetAdmin(string id)
        {
            Instance.Admin.Add(id);
        }

        public static void SetSuperAdmin(string id)
        {
            Instance.SuperAdmin.Add(id);
        }
    }
}
