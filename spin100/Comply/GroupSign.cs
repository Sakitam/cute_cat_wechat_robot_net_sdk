﻿//==============================================================
//  Create by Sakitam_Ins at 2020/2/1 16:52:56.
//  Version 1.0
//  Sakitam_Ins [tjwsq-qiqiqi@outlook.com]
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace spin100_Keaimao_SDK.Comply
{
    public class GroupSign
    {
        public string GroupId { get; set; }
        public string AdminId { get; set; }
        public string TotleNum { get; set; }
        public string CurNum { get; set; }
        public bool IsOpen { get; set; }
        public List<string> NameList { get; set; }
        
        public List<string> SignedNameList { get; set; }
    }
}
