﻿//==============================================================
//  Create by Sakitam_Ins at 2020/2/1 21:16:40.
//  Version 1.0
//  Sakitam_Ins [tjwsq-qiqiqi@outlook.com]
//==============================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace spin100_Keaimao_SDK.Comply
{
    public class ConfigInstance
    {
        public List<GroupSign> GroupSignsList { get; set; }
        public List<string> Admin { get; set; }
        public List<string> SuperAdmin { get; set; }
    }
}
